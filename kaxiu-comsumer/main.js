import Vue from 'vue'
import App from './App'

import store from './store'

import api from '@/common/vmeitime-http/'

// 自定义导航栏组件
import cuCustom from './colorui/components/cu-custom.vue'
Vue.component('cu-custom',cuCustom)

Vue.config.productionTip = true

Vue.prototype.$store = store


// 全局挂载后使用
Vue.prototype.$api = api

App.mpType = 'app'

const app = new Vue({
    store,
    ...App
})
app.$mount()
