/*
Navicat Premium Data Transfer

Source Server         : localhost
Source Server Type    : MySQL
Source Server Version : 50723
Source Host           : 127.0.0.1:3306
Source Schema         : kaxiu

Target Server Type    : MySQL
Target Server Version : 50723
File Encoding         : 65001

Date: 12/08/2019 21:09:01
*/

SET NAMES utf8mb4;
SET FOREIGN_KEY_CHECKS = 0;

-- ----------------------------
-- Table structure for account
-- ----------------------------
DROP TABLE IF EXISTS `account`;
CREATE TABLE `account` (
 `id` bigint(20) NOT NULL COMMENT '主键ID',
 `user_id` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '用户id',
 `amount` decimal(20,6) DEFAULT 0 COMMENT '可用金额',
 `frozen_mount` decimal(20,6) DEFAULT 0 COMMENT '冻结金额',
 `withdraw` decimal(20,6) DEFAULT 0 COMMENT '已提现金额',
 `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
 `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
 `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
 `status` tinyint(3) DEFAULT '1' COMMENT '状态:1正常,2:冻结',
 `type` tinyint(3) DEFAULT '1' COMMENT '账户类型: 1:个人账户',
 PRIMARY KEY (`id`) USING BTREE,
 UNIQUE KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='资金账户表';

INSERT INTO `kaxiu`.`account`(`id`, `user_id`, `amount`, `frozen_mount`, `withdraw`, `created_time`, `updated_time`, `deleted`, `status`, `type`) VALUES (1, '1', 0, 0, 0, '2019-08-18 15:07:23', NULL, 0, 1, 1);
INSERT INTO `kaxiu`.`account`(`id`, `user_id`, `amount`, `frozen_mount`, `withdraw`, `created_time`, `updated_time`, `deleted`, `status`, `type`) VALUES (2, '2', 0.000000, 0, 0, '2019-08-18 15:24:37', '2019-08-18 17:07:18', 0, 1, 1);
INSERT INTO `kaxiu`.`account`(`id`, `user_id`, `amount`, `frozen_mount`, `withdraw`, `created_time`, `updated_time`, `deleted`, `status`, `type`) VALUES (1160912794938245122, '1160912794896302082', 0, 0, 0, '2019-08-12 13:55:53', NULL, 0, 1, 1);
INSERT INTO `kaxiu`.`account`(`id`, `user_id`, `amount`, `frozen_mount`, `withdraw`, `created_time`, `updated_time`, `deleted`, `status`, `type`) VALUES (1162292432218431490, '1162292432172294146', 0, 0, 0, '2019-08-16 09:18:04', NULL, 0, 1, 1);
INSERT INTO `kaxiu`.`account`(`id`, `user_id`, `amount`, `frozen_mount`, `withdraw`, `created_time`, `updated_time`, `deleted`, `status`, `type`) VALUES (1162306000561123330, '1162306000523374593', 0, 0, 0, '2019-08-16 10:11:59', NULL, 0, 1, 1);

-- ----------------------------
-- Table structure for account_log
-- ----------------------------
DROP TABLE IF EXISTS `account_log`;
CREATE TABLE `account_log` (
     `id` bigint(20) NOT NULL COMMENT '主键ID',
     `account_id` bigint(20) NOT NULL COMMENT '账户编号',
     `remark` varchar(200) DEFAULT NULL COMMENT '备注',
     `amount` decimal(20,6) NOT NULL COMMENT '变动金额',
     `fund_direction` tinyint(3) NOT NULL COMMENT '流向,流入：1，流出：2',
     `biz_type` varchar(36) NOT NULL COMMENT '变更业务类型：支付：1，提现：2',
     `biz_id` varchar(36) NOT NULL COMMENT '变更关联的业务id',
     `transaction_id` varchar(200) DEFAULT NULL COMMENT '支付交易id',
     `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
     `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
     `status` tinyint(3) DEFAULT '1' COMMENT '状态',
     PRIMARY KEY (`id`) USING BTREE,
     KEY `idx_account_id` (`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='资金账户记录表';

-- ----------------------------
-- Table structure for account_withdraw
-- ----------------------------
DROP TABLE IF EXISTS `account_withdraw`;
CREATE TABLE `account_withdraw` (
          `id` bigint(20) NOT NULL COMMENT '主键ID',
          `account_id` bigint(20) NOT NULL COMMENT '账户编号',
          `remark` varchar(200) DEFAULT NULL COMMENT '备注',
          `amount` decimal(20,6) NOT NULL COMMENT '提现金额金额',
          `transaction_id` varchar(200) DEFAULT NULL COMMENT '支付交易id',
          `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
          `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
          `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
          `status` tinyint(3) DEFAULT '1' COMMENT '状态:审核中,已打款,已到账',
          `approver_id` bigint(20) DEFAULT NULL COMMENT '审批人id',
          `approver_name` varchar(50) DEFAULT NULL COMMENT '审批人姓名',
          PRIMARY KEY (`id`) USING BTREE,
          KEY `idx_account_id` (`account_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='提现';

-- ----------------------------
-- Table structure for attestation
-- ----------------------------
DROP TABLE IF EXISTS `attestation`;
CREATE TABLE `attestation` (
     `id` bigint(20) NOT NULL COMMENT 'ID',
     `user_id` bigint(20) NOT NULL COMMENT '用户 ID',
     `audit_type` tinyint(3) NOT NULL DEFAULT '1' COMMENT '认证类型：维修员：1、商户：2、代理：3',
     `real_name` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '真实姓名，如果是商户就是商户名称',
     `audit_info` varchar(1000) COLLATE utf8mb4_bin NOT NULL COMMENT '认证信息',
     `audit_status` tinyint(3) DEFAULT '3' COMMENT '用户认证状态，1：已审核，0：审核失败，3：审核中',
     `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
     `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
     `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
     `status` tinyint(3) DEFAULT '1' COMMENT '状态',
     PRIMARY KEY (`id`) USING BTREE,
     KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='认证记录';

-- ----------------------------
-- Records of attestation
-- ----------------------------
BEGIN;
INSERT INTO `attestation` VALUES (1160898349784629249, 1159748975545335809, 1, '123', '{\"auditType\":1,\"idCard\":\"12321321\",\"imgBackPath\":\"wx0b4c7b7e9defd6e5.o6zAJs8j5u5v560EaSZ1wv-XA7QQ.ZcpAEwz0lMJT203232245a60b63c45baeba0c590e7f2.png\",\"imgFrontPath\":\"wx0b4c7b7e9defd6e5.o6zAJs8j5u5v560EaSZ1wv-XA7QQ.n4jbvzKiJYa981f43db7c65a08193c451bec0100b7ba.png\",\"imgHandheldPath\":\"wx0b4c7b7e9defd6e5.o6zAJs8j5u5v560EaSZ1wv-XA7QQ.deGWgVzaZqa781f43db7c65a08193c451bec0100b7ba.png\",\"name\":\"123\"}', 0, '2019-08-12 12:58:29', NULL, 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for basic_user
-- ----------------------------
DROP TABLE IF EXISTS `basic_user`;
CREATE TABLE `basic_user` (
    `id` bigint(20) NOT NULL COMMENT 'ID',
    `nickname` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '昵称',
    `real_name` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '真实姓名',
    `avatar` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '头像',
    `gender` tinyint(3) DEFAULT '0' COMMENT '性别:0未知;1男;2女',
    `open_id` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信 openid',
    `union_id` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信 unionid',
    `app_id` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '微信 APP ID',
    `mobile` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '手机号',
    `language` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '语言',
    `city` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'city',
    `province` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'province',
    `country` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT 'country',
    `password` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '登陆密码，管理端用户使用',
    `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
    `status` tinyint(3) DEFAULT '1' COMMENT '状态',
    `audit_status` tinyint(3) DEFAULT NULL COMMENT '用户认证状态，1：已认证，0：未认证，3：认证中',
    PRIMARY KEY (`id`) USING BTREE,
--     UNIQUE KEY `idx_mobile` (`mobile`) USING BTREE,
    UNIQUE KEY `idx_user_open_id` (`open_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='用户信息';

-- ----------------------------
-- Records of basic_user
-- ----------------------------
BEGIN;
INSERT INTO `basic_user` VALUES (1, '李阳', NULL, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', 1, 'oqDsT0bvdzHNX3hBPDktUrmIPElk', NULL, 'wx5dc5c3f431a47092', '12312312312', 'zh_CN', '', '', 'Bermuda', '123456', '2019-08-06 17:54:32', '2019-08-08 17:50:50', 0, 1, NULL);
INSERT INTO `basic_user` VALUES (1159748975545335809, '李阳', NULL, 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTJt18m6r8LJWyEgc6rk5OLvznPbpXJhIjRnpsY8TvPC4fSVY14YR6giaQKXqMh4icXnVrzR9pzMN8Iw/132', 1, 'onuj94xqNIKA4-AR6z-wEoX2KaO4', NULL, 'wx0b4c7b7e9defd6e5', NULL, 'zh_CN', '', '', 'Bermuda', NULL, '2019-08-09 08:51:17', '2019-08-09 10:48:23', 0, 1, 0);
COMMIT;

-- ----------------------------
-- Table structure for car_info
-- ----------------------------
DROP TABLE IF EXISTS `car_info`;
CREATE TABLE `car_info` (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `user_id` bigint(20) NOT NULL COMMENT '用户id',
  `model` varchar(50) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '车辆型号',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
  `status` tinyint(3) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='车辆信息\n';

-- ----------------------------
-- Table structure for order_item
-- ----------------------------
DROP TABLE IF EXISTS `order_item`;
CREATE TABLE `order_item` (
    `id` bigint(20) NOT NULL COMMENT 'ID',
    `user_id` bigint(20) NOT NULL COMMENT '用户id',
    `order_id` bigint(20) NOT NULL COMMENT '订单id',
    `product_id` bigint(20) NOT NULL COMMENT '维修类别(商品)id',
    `amount` decimal(20,6) NOT NULL COMMENT '价格',
    `remark` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
    `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
    `status` tinyint(3) DEFAULT '1' COMMENT '状态',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单项';

-- ----------------------------
-- Table structure for orders
-- ----------------------------
DROP TABLE IF EXISTS `orders`;
CREATE TABLE `orders` (
`id` bigint(20) NOT NULL COMMENT 'ID',
`user_id` bigint(20) NOT NULL COMMENT '用户表id',
`amount` decimal(20,6) NOT NULL COMMENT '价格',
`real_amount` decimal(20,6) NOT NULL COMMENT '实际支付价格',
`remark` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
`pay_status` int(1) DEFAULT '0' COMMENT '0:未支付，1:已支付,2:已关闭;3,支付失败',
`transaction_id` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '支付交易id',
`finished_time` datetime DEFAULT NULL COMMENT '订单完成时间',
`created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
`updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
`deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
`status` tinyint(3) DEFAULT '1' COMMENT '状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单',
`order_type` tinyint(3) DEFAULT '1' COMMENT '订单类型:维修订单',
PRIMARY KEY (`id`) USING BTREE,
KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='订单表';

-- ----------------------------
-- Table structure for repair_order
-- ----------------------------
DROP TABLE IF EXISTS `repair_order`;
CREATE TABLE `repair_order` (
      `id` bigint(20) NOT NULL COMMENT 'ID',
      `order_id` bigint(20) NOT NULL COMMENT 'order id',
      `user_id` bigint(20) NOT NULL COMMENT '用户 id,下单用户',
      `maintenance_id` bigint(20) DEFAULT NULL COMMENT '当前维修人员 id',
      `service_id` bigint(20) NOT NULL COMMENT '维修类别(商品)id',
      `amount` decimal(20,6) NOT NULL COMMENT '总费用价格',
      `scene_photo` varchar(1000) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '现场照片,微信图片，最多3张',
      `before_repair_photo` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '维修员到场拍的照片',
      `after_repair_photo` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '维修结束拍的照片',
      `contact` varchar(20) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人',
      `mobile` varchar(11) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '联系人手机号',
      `remark` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
      `review` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '评价',
      `star_level` int(1) DEFAULT NULL COMMENT '评分, 0-5',
      `location` varchar(300) COLLATE utf8mb4_bin NOT NULL COMMENT '订单位置：经纬度、速度、精确度、高度、垂直精度、水平精度.JSON',
      `personal_location` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '维修人员位置：经纬度、速度、精确度、高度、垂直精度、水平精度.JSON',
      `finished_time` datetime DEFAULT NULL COMMENT '服务结束时间',
      `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
      `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
      `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
      `status` tinyint(3) DEFAULT '1' COMMENT '状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单',
      PRIMARY KEY (`id`) USING BTREE,
      KEY `idx_user_id` (`user_id`) USING BTREE,
      KEY `idx_maintenance_id` (`maintenance_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='维修单详情';

-- ----------------------------
-- Table structure for send_order
-- ----------------------------
DROP TABLE IF EXISTS `send_order`;
CREATE TABLE `send_order` (
    `id` bigint(20) NOT NULL COMMENT 'ID',
    `repair_order_id` bigint(20) NOT NULL COMMENT '维修单id',
    `user_id` bigint(20) NOT NULL COMMENT '用户 id,下单用户',
    `remark` varchar(200) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '备注',
    `maintenance_id` bigint(20) DEFAULT NULL COMMENT '维修人员 id',
    `finished_time` datetime DEFAULT NULL COMMENT '服务结束时间',
    `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
    `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
    `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
    `status` tinyint(3) DEFAULT '1' COMMENT '状态，1：发起订单，未支付，2：已支付，等待接单，3：已接单，正在赶往现场，4：维修完成，5：取消订单',
    PRIMARY KEY (`id`) USING BTREE,
    KEY `idx_user_id` (`user_id`) USING BTREE,
    KEY `idx_maintenance_id` (`maintenance_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='维修-派单记录';

-- ----------------------------
-- Table structure for service_category
-- ----------------------------
DROP TABLE IF EXISTS `service_category`;
CREATE TABLE `service_category` (
          `id` bigint(20) NOT NULL COMMENT 'ID',
          `parent_id` bigint(20) DEFAULT NULL COMMENT '父维修类别id',
          `name` varchar(50) COLLATE utf8mb4_bin NOT NULL COMMENT '类别名称',
          `pinyin` varchar(100) COLLATE utf8mb4_bin NOT NULL COMMENT '类别拼音（小程序顶部菜单栏滚动使用）',
          `icon` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '菜单图标',
          `descripion` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
          `amount` decimal(20,6) NOT NULL COMMENT '价格',
          `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
          `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
          `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
          `status` tinyint(3) DEFAULT '1' COMMENT '启用,0:未启用,1:启用',
          PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='维修类别(商品)表';

-- ----------------------------
-- Records of service_category
-- ----------------------------
BEGIN;
INSERT INTO `service_category` VALUES (1, NULL, '维修类型1', 'weixiu1', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：100，油钱：20，人工费：33', 120.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:31', 0, 1);
INSERT INTO `service_category` VALUES (2, 1, '维修类型2', 'weixiu2', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：200，油钱：20，人工费：33', 220.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:32', 0, 1);
INSERT INTO `service_category` VALUES (3, NULL, '维修类型3', 'weixiu3', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：400，油钱：20，人工费：33', 330.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:33', 0, 1);
INSERT INTO `service_category` VALUES (4, 1, '维修类型4', 'weixiu4', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：400，油钱：20，人工费：33', 630.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:38', 0, 1);
INSERT INTO `service_category` VALUES (5, NULL, '维修类型5', 'weixiu5', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：400，油钱：20，人工费：33', 10.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:39', 0, 1);
INSERT INTO `service_category` VALUES (6, 1, '维修类型6', 'weixiu6', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：400，油钱：20，人工费：33', 10.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:40', 0, 1);
INSERT INTO `service_category` VALUES (7, NULL, '维修类型7', 'weixiu7', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：400，油钱：20，人工费：33', 10.000000, '2019-08-01 13:21:28', '2019-08-01 18:37:37', 0, 1);
INSERT INTO `service_category` VALUES (8, 2, '维修类型8', 'weixiu8', 'https://wx.qlogo.cn/mmopen/vi_32/Q0j4TwGTfTIOiaeiaQt01o9iawzFicCgsiaM1Jiao1uQhlufJnXz0CudhoDiabfXuYnurkUxGhp6JZ4BicoecuYp0OZoicQ/132', '轮胎：400，油钱：20，人工费：33', 10.000000, '2019-08-01 13:21:28', '2019-08-01 18:51:43', 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for service_personal
-- ----------------------------
DROP TABLE IF EXISTS `service_personal`;
CREATE TABLE `service_personal` (
          `id` bigint(20) NOT NULL COMMENT 'ID',
          `user_id` bigint(20) NOT NULL COMMENT '关联微信用户Open ID',
          `order_number` bigint(10) NOT NULL COMMENT '接单数量',
          `star_level_total` tinyint(3) DEFAULT NULL COMMENT '累计评分',
          `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
          `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
          `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
          `service_start_time` timestamp NULL DEFAULT NULL COMMENT '服务开始时间',
          `service_end_time` timestamp NULL DEFAULT NULL COMMENT '服务结束时间',
          `service_location` varchar(100) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '接单范围',
          `status` tinyint(3) DEFAULT '1' COMMENT '状态,1:未上线:2:空闲,3:服务中',
          PRIMARY KEY (`id`) USING BTREE,
          UNIQUE KEY `idx_user_id` (`user_id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='维修人员信息表';

-- ----------------------------
-- Table structure for sys_dic
-- ----------------------------
DROP TABLE IF EXISTS `sys_dic`;
CREATE TABLE `sys_dic` (
   `id` bigint(20) NOT NULL COMMENT 'ID',
   `dic_key` varchar(30) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典key',
   `dic_value` varchar(300) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '字典value',
   `parent_id` bigint(20) DEFAULT NULL COMMENT 'parent id',
   `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
   `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
   `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
   `status` tinyint(3) DEFAULT '1' COMMENT '状态,1:启用，2：未启用',
   `description` varchar(255) COLLATE utf8mb4_bin DEFAULT NULL COMMENT '描述',
   PRIMARY KEY (`id`) USING BTREE
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_bin COMMENT='字典表';

INSERT INTO `sys_dic` VALUES (1, 'tip', '91', NULL, '2019-08-04 12:43:37', '2019-08-18 17:48:34', 0, 1, '跑腿费');
INSERT INTO `sys_dic` VALUES (2, 'amapKey', 'a00412fadf3cfb863ee24544cede5edc', NULL, '2019-08-06 18:16:40', '2019-08-18 17:48:46', 0, 1, '高德地图使用');
INSERT INTO `sys_dic` VALUES (3, 'grabSingle', '请确认您的信息无误后抢单', NULL, '2019-08-16 17:31:36', '2019-08-18 17:48:54', 0, 1, '确认抢单提示语');
INSERT INTO `sys_dic` VALUES (4, 'newOrdersVoice', 'https://img-cdn-qiniu.dcloud.net.cn/uniapp/audio/music.mp3', NULL, '2019-08-16 18:05:31', '2019-08-18 17:49:01', 0, 1, '新订单播放语音地址');
INSERT INTO `sys_dic` VALUES (5, 'rake', '5', NULL, '2019-08-18 17:47:11', '2019-08-18 17:49:19', 0, 1, '平台方服务费抽成，百分比，不得大于百分之百');


-- ----------------------------
-- Table structure for sys_permission_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_permission_role`;
CREATE TABLE `sys_permission_role` (
             `id` bigint(20) NOT NULL,
             `role_id` varchar(255) DEFAULT NULL COMMENT '角色id',
             `permission_id` varchar(255) DEFAULT NULL COMMENT '权限id',
             PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色权限中间表';

-- ----------------------------
-- Records of sys_permission_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_permission_role` VALUES (1, '1', '1');
INSERT INTO `sys_permission_role` VALUES (2, '1', '2');
INSERT INTO `sys_permission_role` VALUES (3, '1', '3');
INSERT INTO `sys_permission_role` VALUES (4, '1', '4');
INSERT INTO `sys_permission_role` VALUES (5, '1', '5');
INSERT INTO `sys_permission_role` VALUES (6, '1', '6');
INSERT INTO `sys_permission_role` VALUES (7, '1', '7');
COMMIT;

-- ----------------------------
-- Table structure for sys_premission
-- ----------------------------
DROP TABLE IF EXISTS `sys_premission`;
CREATE TABLE `sys_premission` (
        `id` bigint(20) NOT NULL COMMENT 'id',
        `name` varchar(255) DEFAULT NULL COMMENT '权限名称',
        `descripion` varchar(255) DEFAULT NULL COMMENT '权限描述',
        `url` varchar(255) DEFAULT NULL COMMENT '授权链接',
        `pid` varchar(255) DEFAULT NULL COMMENT '父节点id',
        `perms` varchar(255) DEFAULT NULL COMMENT '权限标识',
        `type` tinyint(3) DEFAULT NULL COMMENT '类型   0：目录   1：菜单   2：按钮',
        `icon` varchar(255) DEFAULT NULL COMMENT '菜单图标',
        `order_num` int(11) DEFAULT NULL COMMENT '排序',
        `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
        `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
        `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
        `status` tinyint(3) DEFAULT '1' COMMENT '状态',
        PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='权限表';

-- ----------------------------
-- Records of sys_premission
-- ----------------------------
BEGIN;
INSERT INTO `sys_premission` VALUES (1, '管理首页', '', '/pages/home', NULL, 'system:home', 0, NULL, NULL, '2019-07-31 16:18:18', '2019-08-08 18:28:39', 0, NULL);
INSERT INTO `sys_premission` VALUES (2, '管理用户', '', '/pages/member', NULL, 'system:member', 1, NULL, NULL, '2019-07-31 16:18:39', '2019-08-08 18:28:41', 0, NULL);
INSERT INTO `sys_premission` VALUES (3, '管理订单', '', '/pages/order', NULL, 'system:order', 1, NULL, NULL, '2019-07-31 16:20:57', '2019-08-08 18:28:43', 0, NULL);
INSERT INTO `sys_premission` VALUES (4, '管理权限', '', '/pages/permission', NULL, 'system:permission', 1, NULL, NULL, '2019-07-31 16:21:19', '2019-08-08 18:28:45', 0, NULL);
INSERT INTO `sys_premission` VALUES (5, '管理系统', '', '/pages/system', NULL, 'system:opration', 1, NULL, NULL, '2019-07-31 16:22:06', '2019-08-08 18:28:47', 0, NULL);
INSERT INTO `sys_premission` VALUES (6, '用户首页', '', '/kaxiu', NULL, 'consumer:home', NULL, NULL, NULL, '2019-08-07 11:34:17', '2019-08-08 18:28:49', 0, 1);
INSERT INTO `sys_premission` VALUES (7, '用户下单', '', '/kaxiu/orders', NULL, 'consumer:takeOrder', NULL, NULL, NULL, '2019-08-08 18:12:45', '2019-08-08 18:28:51', 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role
-- ----------------------------
DROP TABLE IF EXISTS `sys_role`;
CREATE TABLE `sys_role` (
  `id` bigint(20) NOT NULL COMMENT 'id',
  `name` varchar(255) DEFAULT NULL COMMENT '角色名称',
  `created_time` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP COMMENT '创建时间',
  `updated_time` timestamp NULL DEFAULT NULL ON UPDATE CURRENT_TIMESTAMP COMMENT '更新时间',
  `deleted` tinyint(3) DEFAULT '0' COMMENT '是否删除,0:未删除,1:删除',
  `status` tinyint(3) DEFAULT '1' COMMENT '状态',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='角色表';

-- ----------------------------
-- Records of sys_role
-- ----------------------------
BEGIN;
INSERT INTO `sys_role` VALUES (1, 'administer', '2019-07-31 16:08:54', '2019-08-07 11:30:45', 0, NULL);
INSERT INTO `sys_role` VALUES (2, 'kaxiu-consumer', '2019-08-07 11:31:20', NULL, 0, 1);
INSERT INTO `sys_role` VALUES (3, 'kaxiu-service', '2019-08-07 11:31:30', NULL, 0, 1);
INSERT INTO `sys_role` VALUES (4, 'test', '2019-08-08 15:27:42', NULL, 0, 1);
COMMIT;

-- ----------------------------
-- Table structure for sys_role_user
-- ----------------------------
DROP TABLE IF EXISTS `sys_role_user`;
CREATE TABLE `sys_role_user` (
       `id` bigint(20) NOT NULL,
       `sys_user_id` varchar(255) DEFAULT NULL COMMENT 'user id',
       `sys_role_id` varchar(255) DEFAULT NULL COMMENT '角色id',
       PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COMMENT='用户角色中间表';

-- ----------------------------
-- Records of sys_role_user
-- ----------------------------
BEGIN;
INSERT INTO `sys_role_user` VALUES (1, '1', '1');
INSERT INTO `sys_role_user` VALUES (2, '1', '2');
INSERT INTO `sys_role_user` VALUES (3, '1', '3');
INSERT INTO `sys_role_user` VALUES (1159748975570501633, '1159748975545335809', '3');
COMMIT;

SET FOREIGN_KEY_CHECKS = 1;
