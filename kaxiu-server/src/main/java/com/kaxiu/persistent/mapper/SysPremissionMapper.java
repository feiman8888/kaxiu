package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.SysPremission;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * <p>
 * 权限表 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface SysPremissionMapper extends BaseMapper<SysPremission> {

    List<SysPremission> selectByRoleId(@Param("roleId") Long rolid);
}
