package com.kaxiu.persistent.mapper;

import com.kaxiu.persistent.entity.AccountWithdraw;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 * 提现 Mapper 接口
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
public interface AccountWithdrawMapper extends BaseMapper<AccountWithdraw> {

}
