package com.kaxiu.persistent.entity;

import com.kaxiu.common.base.BaseEntity;
import java.time.LocalDateTime;
import lombok.Data;
import lombok.EqualsAndHashCode;
import lombok.experimental.Accessors;

/**
 * <p>
 * 维修-派单记录
 * </p>
 *
 * @author ly
 * @since 2019-08-03
 */
@Data
@EqualsAndHashCode(callSuper = true)
@Accessors(chain = true)
public class SendOrder extends BaseEntity {

private static final long serialVersionUID=1L;

    /**
     * 维修单id
     */
    private Long repairOrderId;

    /**
     * 用户 openid,下单用户
     */
    private Long userId;

    /**
     * 备注
     */
    private String remark;

    /**
     * 维修人员 id
     */
    private Long maintenanceId;

    /**
     * 服务结束时间
     */
    private LocalDateTime finishedTime;

}
