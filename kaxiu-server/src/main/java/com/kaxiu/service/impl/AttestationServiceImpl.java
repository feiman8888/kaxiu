package com.kaxiu.service.impl;

import com.alibaba.fastjson.JSON;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.config.shiro.jwt.JwtConfig;
import com.kaxiu.config.shiro.jwt.JwtToken;
import com.kaxiu.persistent.entity.Attestation;
import com.kaxiu.persistent.entity.BasicUser;
import com.kaxiu.persistent.mapper.AttestationMapper;
import com.kaxiu.persistent.mapper.BasicUserMapper;
import com.kaxiu.service.IAttestationService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kaxiu.vo.AttestationVo;
import com.kaxiu.vo.enums.AttestationEnum;
import org.apache.shiro.SecurityUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 认证记录 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class AttestationServiceImpl extends ServiceImpl<AttestationMapper, Attestation> implements IAttestationService {

    @Autowired
    private BasicUserMapper userMapper;

    @Autowired
    private AttestationMapper attestationMapper;

    @Autowired
    private RedisService redisService;


    @Override
    public int saveAttestation(AttestationVo vo) {
        BasicUser user = redisService.getWxUser();
        Attestation attestation = new Attestation();
        attestation.setAuditInfo(JSON.toJSONString(vo));
        attestation.setUserId(user.getId());
        attestation.setAuditType(vo.getAuditType());
        attestation.setRealName(vo.getName());
        attestation.setAuditStatus(AttestationEnum.CERTIFICATING);

        //修改用户信息中的认证状态
        user.setAuditStatus(AttestationEnum.CERTIFICATING);
        user.setMobile(vo.getMobile());

        //修改user后需要将user重新更新到redis，类似于springcache的作用
        if(userMapper.updateById(user) > 0){
            redisService.setWxUser(user);
        }
        return attestationMapper.insert(attestation);
    }
}
