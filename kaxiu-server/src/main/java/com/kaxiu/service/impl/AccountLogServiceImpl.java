package com.kaxiu.service.impl;

import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.kaxiu.config.CommonConstant;
import com.kaxiu.persistent.entity.AccountLog;
import com.kaxiu.persistent.entity.RepairOrder;
import com.kaxiu.persistent.mapper.AccountLogMapper;
import com.kaxiu.service.IAccountLogService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;

/**
 * <p>
 * 资金账户记录表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class AccountLogServiceImpl extends ServiceImpl<AccountLogMapper, AccountLog> implements IAccountLogService {

    @Autowired
    private AccountLogMapper mapper;

    @Override
    public AccountLog getById(String transactionId, Long accountId) {
        return mapper.selectOne(Wrappers.<AccountLog>lambdaQuery()
                .eq(AccountLog::getAccountId, accountId)
                .eq(AccountLog::getTransactionId, transactionId)
        );
    }
}
