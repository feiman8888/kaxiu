package com.kaxiu.service.impl;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.conditions.update.UpdateWrapper;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.baomidou.mybatisplus.core.toolkit.Wrappers;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import com.kaxiu.config.CommonConstant;
import com.kaxiu.config.redis.RedisService;
import com.kaxiu.persistent.entity.*;
import com.kaxiu.persistent.mapper.OrderItemMapper;
import com.kaxiu.persistent.mapper.OrdersMapper;
import com.kaxiu.persistent.mapper.RepairOrderMapper;
import com.kaxiu.persistent.mapper.SendOrderMapper;
import com.kaxiu.service.IOrdersService;
import com.kaxiu.service.IRepairOrderService;
import com.kaxiu.vo.order.OrderRequestVo;
import com.kaxiu.vo.order.OrderResponseVo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.io.Serializable;
import java.time.LocalDateTime;
import java.util.List;

/**
 * <p>
 * 订单表 服务实现类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
@Service
public class RepairOrderServiceImpl extends ServiceImpl<RepairOrderMapper, RepairOrder> implements IRepairOrderService {

    @Autowired
    private RepairOrderMapper mapper;

    @Override
    public RepairOrder getByOrderId(Long orderId) {
        return mapper.selectOne(Wrappers.<RepairOrder>lambdaQuery().eq(RepairOrder::getOrderId, orderId));
    }
}
