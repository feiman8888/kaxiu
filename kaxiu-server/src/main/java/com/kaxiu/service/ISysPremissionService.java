package com.kaxiu.service;

import com.kaxiu.persistent.entity.SysPremission;
import com.baomidou.mybatisplus.extension.service.IService;

import java.util.List;

/**
 * <p>
 * 权限表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ISysPremissionService extends IService<SysPremission> {

    List<SysPremission> getByRoleId(Long rolid);
}
