package com.kaxiu.service;

import com.kaxiu.persistent.entity.SysPermissionRole;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色权限中间表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface ISysPermissionRoleService extends IService<SysPermissionRole> {

}
