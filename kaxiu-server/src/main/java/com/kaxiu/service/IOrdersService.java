package com.kaxiu.service;

import com.alibaba.fastjson.JSONObject;
import com.baomidou.mybatisplus.core.metadata.IPage;
import com.kaxiu.persistent.entity.Orders;
import com.baomidou.mybatisplus.extension.service.IService;
import com.kaxiu.vo.order.OrderRequestVo;
import com.kaxiu.vo.order.OrderResponseVo;

import java.io.Serializable;
import java.util.List;

/**
 * <p>
 * 订单表 服务类
 * </p>
 *
 * @author ly
 * @since 2019-08-04
 */
public interface IOrdersService extends IService<Orders> {


    /**
     * consumer下单
     * @param vo
     * @return
     */
    Serializable takeOrder(OrderRequestVo vo);

    /**
     * server接单
     * @param location
     * @return
     */
    int takeOrder(Long orderId, JSONObject location);

    /**
     * consumer
     * 根据用户id 获取订单数据
     * @return
     */
    IPage<OrderResponseVo> getOrderDetailByUserId(Integer status,
                                                  Integer pageSize, Integer pageIndex);

    /**
     * consumer
     * 根据用户id 获取订单数据
     * @param orderId
     * @return
     */
    OrderResponseVo getOrderDetailById(Serializable orderId);

    /**
     * consumer评论
     * @param orderId
     * @param rate
     * @param review
     * @return
     */
    int rateById(Serializable orderId, Integer rate, String review);

    /**
     * server 获取有效订单
     * @return
     */
    List<OrderResponseVo> getEffectiveOrders();

    /**
     * 获取正在进行中的订单
     * @return
     */
    OrderResponseVo getServerOrder();

    /**
     * server 根据状态获取订单
     * @return
     * @param status
     * @param pageIndex
     * @param pageSize
     */
    IPage<OrderResponseVo> getServerOrder(Integer status, Integer pageIndex, Integer pageSize);

    /**
     * server 结束订单
     * @param orderId
     * @param imgFrontPath  维修前照片
     * @param imgOverPath 维修后照片
     * @return
     */
    boolean overOrder(String orderId, String imgFrontPath, String imgOverPath);

    Boolean paySuccess(Long orderId);
}
