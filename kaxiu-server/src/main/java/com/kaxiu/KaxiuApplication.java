package com.kaxiu;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.EnableAspectJAutoProxy;

@SpringBootApplication
public class KaxiuApplication {

	public static void main(String[] args) {
		SpringApplication.run(KaxiuApplication.class, args);
	}

}
